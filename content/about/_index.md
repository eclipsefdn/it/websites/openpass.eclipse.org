---
title: About openPASS
seo_title: About | openPASS
description: >
  Harmonized and flexible platform for scenario-based traffic simulation of
  advanced driver assistance systems and automated driving systems.
keywords:
  - open source
  - use cases
  - open platform, assessment of safety systems
  - target objectives
  - traffic scenarios
  - simulations
  - eclipse foundation
date: 2020-05-19T14:08:12-04:00
hide_sidebar: true
hide_page_title: true
page_css_file: /css/pages/about.css
layout: single
cascade:
  format_title: false # Prevents title case on "openPASS"
---

{{< pages/about >}}

