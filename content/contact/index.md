---
title: "Contact"
seo_title: Contact | openPASS | Eclipse Foundation
date: 2019-09-13T14:08:12-04:00
description: ""
categories: []
keywords: []
slug: ""
aliases: []
toc: false
draft: false
hide_sidebar: true
hide_page_title: true
---

# Contact us about Membership { .padding-top-60 }

The openPASS Working Group is open for all new interested members. Contact us
if your organization is interested in participating in the development of the 
target objectives of openPASS. By joining you can discuss with us the roadmap 
and collaborate actively with the working group.

{{<bootstrap/button href="https://membership.eclipse.org/application#sign-in" pClass="text-center padding-20" linkClass="btn-secondary btn-outlined padding-left-40 padding-right-40">}}
Join openPASS
{{</bootstrap/button>}}

In order to participate in the openPASS Working Group, an entity must be at
least a member of the Eclipse Foundation. The annual membership fee is tiered based on revenue and is determined as described
in the Eclipse Bylaws and is listed in the Eclipse Membership Agreement.

In the openPASS charter you can find additional information about the
membership fee structure and membership privileges.

For addressing especially developers of openPASS, contact or subscribe to the
[developer mailing list](https://accounts.eclipse.org/mailing-list/openpass-dev).

**Eclipse Foundation:**  

Michael Plagge

Community Development

Mail: michael.plagge@eclipse-foundation.org

**openPASS Steering Committee**

Mail: openpass-sc@eclipse.org
