---
title: "Home"
seo_title: "openPASS Working Group"
headline: "<span>openPASS Working Group</span>" # Spans are used to prevent title case
description: "The rise of advanced driver assistance systems and automated driving systems leads to the need of virtual simulation to assess these systems and their effects."
tagline: "The rise of advanced driver assistance systems and automated driving systems leads to the need of virtual simulation to assess these systems and their effects."
hide_page_title: true
hide_sidebar: true
hide_breadcrumb: true
date: 2019-09-10T15:50:25-04:00
page_css_file: /css/pages/home.css
layout: "single"
container: "container-fluid"
---

<!-- OpenPASS section -->

{{< pages/home/openpass-section >}}

The openPASS (**open** **P**latform for **A**ssessment of **S**afety **S**ystems)
platform is being developed to conduct scenario-based traffic simulation.
Strong support for simulation standards and a modular setup allow the platform to be tailored to a wide variety of simulation use cases.

{{</ pages/home/openpass-section >}}

<!-- Overview section -->

{{< pages/home/overview-section >}}

<!-- Highlights section -->

{{< pages/home/highlights-section >}}

<!-- Tutorial section -->
{{< pages/home/tutorials-section >}}

<!-- News & Events section -->

{{< grid/section-container class="margin-bottom-30 margin-top-50 news-list" >}}
  {{< grid/div class="col-sm-15 news-list-col" isMarkdown="false" >}}
    <h2 class="heading-line margin-bottom-30">News</h2>
    {{< newsroom/news id="news-template-id" class="news-list" includeList="true" publishTarget="openpass" >}}
  {{</ grid/div >}}
  {{< grid/div class="col-sm-8 col-sm-offset-1 news-list-col" isMarkdown="false" >}}
    <h2 class="heading-line margin-bottom-30">Events</h2>
    {{< newsroom/events
      id="event-list-container"
      publishTarget="openpass"
      class="event-list-custom"
      upcoming="1"
      templateId="custom-events-template"
      templatePath="/js/templates/event-list-format.mustache"
      count="4"
      includeList="true"
    >}}
  {{</ grid/div >}}
{{</ grid/section-container >}}
