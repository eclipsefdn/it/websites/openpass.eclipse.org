---
title: Working Group
seo_title: Working Group | openPASS
keywords:
  - working group
  - timeline
  - members
  - sponsors
  - committee
  - governance
  - charter
  - eclipse foundation
hide_sidebar: true
hide_page_title: true
---

## Scope

The Eclipse Working Group openPASS is managing the Eclipse project openPASS.

Eclipse openPASS provides a software platform that enables the simulation of
traffic scenarios to predict the real-world effectiveness of advanced driver
assistance systems or automated driving systems.

Safety relevant aspects of traffic, such as infrastructural elements or
behavioral characteristics, are investigated with this platform. The platform
can run entirely flexible multi-agent simulations which include stochastic
variation as well as reproducibility. The flexibility is achieved by
outsourcing to dynamic libraries (so called modules) all models defining the
traffic scenario or behavior of traffic participants. Additionally, to the
platform, demonstrator modules are provided, which comprise simple traffic
scenarios and behaviors of traffic participants as well as a collision model
to answer basic research questions. Furthermore, demonstrator applications will
be given to post process the simulation results. openPASS users can incorporate
their own developed modules and post processing applications; this allows them
to answer a wide variety of their specific research questions. These questions
are related to development, testing and rating of vehicle systems. Possible
users can be automotive OEMs, suppliers, public transport authorities, consumer
protection organizations, insurance companies, academia and legislation.

## Timeline

{{< figure class="padding-30" src="/working-group/images/timeline.jpg" alt="Timeline of openPASS working group" >}}

The Eclipse Working Group openPASS was founded in August 2016. The idea of
openPASS was generated from the initiative P.E.A.R.S. The founding members were
BMW Group, Mercedes-Benz-AG, ITK Engineering GmbH and Volkswagen AG. In January
2018 TÜV SÜD Auto Service GmbH has joined the WG followed by Robert Bosch GmbH
in November.

The initial commit was in March 2017. Since then the open source platform has
been developed further and several releases have been published.

## Members

{{< members_list_no_nav >}}

## Governance

| Privilege              | Driver Member | User Member | Service Provider Member | Project Manager |
|------------------------|---------------|-------------|-------------------------|-----------------|
| Steering Committee     | X             | Elected     | Elected                 | -               |
| Architecture Committee | X             | -           | -                       | X               |
| Quality Committee      | X             | Elected     | Elected                 | X               |
| General Assembly       | X             | X           | X                       | -               |

For further information please visit the [Eclipse openPASS Working Group Charter](https://www.eclipse.org/org/workinggroups/openpasswg_charter.php)

## Committees

### Steering Committee  

**Speaker of the Steering Committee:** Jan Dobberstein (Mercedes-Benz AG on behalf of Mercedes-Benz Tech Innovation) 

- BMW Group, Thomas Platzer
- Mercedes-Benz AG on behalf of Mercedes-Benz Tech Innovation, Jan Dobberstein
- Robert Bosch GmbH, Daniel Schmidt
- TÜV SÜD Auto Service GmbH, Vacant 
- Volkswagen AG, Stefan Schoenawa

To contact the openPASS Steering Committee, please write to
<openpass-sc@eclipse.org>

### Architecture Committee  

**Speaker of the Architecture Committee:** Arun Das (BMW Group)

- BMW Group, Arun Das
- Mercedes-Benz AG on behalf of Mercedes-Benz Tech Innovation, Per Lewerenz
- Robert Bosch GmbH, Daniel Schmidt
- TÜV SÜD Auto Service GmbH, Vacant 
- Volkswagen AG, Stefan Schoenawa

Meetings minutes of both Committees can be found on
[GitLab](https://gitlab.eclipse.org/groups/eclipse/openpass/-/wikis/Home/Meeting-Minutes)
